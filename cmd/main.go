package main

import "createmr/internal/application"

func main() {
	app := application.CreateApp()

	app.Run()
}
