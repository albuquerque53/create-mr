package cli

import (
	"createmr/internal/domain/mr"
	"flag"
	"strings"
)

func BuildMRFromArgs() *mr.MR {
	var targetBranch string
	var assignee string
	var reviewer string
	var labels string
	var draft bool

	flag.StringVar(&targetBranch, "t", "production", "Please, specify the target branch (default is 'production').")
	flag.StringVar(&assignee, "a", "", "Please, specify the assignee.")
	flag.StringVar(&reviewer, "r", "", "Please, specify the reviewer.")
	flag.StringVar(&labels, "l", "", "Please, specify the labels.")
	flag.BoolVar(&draft, "d", false, "Please, specify if Merge Request will be open as draft.")
	flag.Parse()

	psdLabels := parseLabels(labels)

	return mr.NewMR(targetBranch, assignee, reviewer, psdLabels, draft)
}

func parseLabels(rawLabels string) []string {
	return strings.Split(rawLabels, ",")
}
