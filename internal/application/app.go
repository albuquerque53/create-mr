package application

import (
	"createmr/internal/application/cli"
	"createmr/internal/domain/mr"
	"createmr/internal/infra/repo"
	"fmt"
	"log"
)

type App struct{}

func CreateApp() *App {
	return &App{}
}

func (a *App) Run() {
	dto := cli.BuildMRFromArgs()

	repo := repo.NewMRRepo()
	mr := mr.NewEntity(repo)

	url, err := mr.CreateMR(dto)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(url)
}
