package mr

type MR struct {
	TargetBranch string
	Assignee     string
	Reviewer     string
	Labels       []string
	Draft        bool
}

func NewMR(targetBranch, assignee, reviewer string, labels []string, draft bool) *MR {
	return &MR{
		targetBranch,
		assignee,
		reviewer,
		labels,
		draft,
	}
}
