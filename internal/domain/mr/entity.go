package mr

import "context"

type Entity struct {
	repo MRRepo
}

func NewEntity(repo MRRepo) *Entity {
	return &Entity{repo}
}

func (e *Entity) CreateMR(mr *MR) (string, error) {
	ctx := context.Background()
	url, err := e.repo.CreateMR(ctx, mr)

	if err != nil {
		return "", err
	}

	err = validateURL(url)

	if err != nil {
		return "", err
	}

	return url, nil
}

func validateURL(url string) error {
	return nil
}
