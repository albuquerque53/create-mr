package mr

import "context"

type MRRepo interface {
	CreateMR(ctx context.Context, mr *MR) (string, error)
}
