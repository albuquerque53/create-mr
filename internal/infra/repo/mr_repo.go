package repo

import (
	"context"
	"createmr/internal/domain/mr"
	"createmr/internal/infra/cmd"
	"fmt"
	"strings"
)

const Git = "git"
const MRTemplate = "push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.target=%s -o merge_request.assign='@%s'"
const Target = "-u origin HEAD"

type MRRepo struct {
	runner *cmd.Runner
}

func NewMRRepo() *MRRepo {
	return &MRRepo{cmd.NewRunner()}
}

func (repo *MRRepo) CreateMR(ctx context.Context, mr *mr.MR) (string, error) {
	cmd := buildMRCommand(mr)

	out, err := repo.runner.Run(Git, strings.Split(cmd, " ")...)

	return string(out), err
}

func buildMRCommand(mr *mr.MR) string {
	cmd := fmt.Sprintf(MRTemplate, mr.TargetBranch, mr.Assignee)

	if mr.Draft {
		cmd += " -o merge_request.draft"
	}

	for _, label := range mr.Labels {
		cmd += fmt.Sprintf(" -o merge_request.label=%s", label)
	}

	return cmd + " " + Target

}
