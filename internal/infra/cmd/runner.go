package cmd

import (
	"bytes"
	"os/exec"
)

type Runner struct {
}

func NewRunner() *Runner {
	return &Runner{}
}

func (r *Runner) Run(command string, args ...string) ([]byte, error) {
	var outb, errb bytes.Buffer

	cmd := exec.Command(command, args...)
	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err := cmd.Run()

	return errb.Bytes(), err
}
